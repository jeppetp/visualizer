var width = window.innerWidth
var height = window.innerHeight

function polar2cartesian(R, theta) {
        x = R * Math.cos(theta);
        y = R * Math.sin(theta);
    return [x,y]
}

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function preload(){
  sound = loadSound('assets/teknotest.wav');
}


function setup(){
  background(0,0,0)
  let cnv = createCanvas(window.innerWidth, window.innerHeight);
  cnv.mouseClicked(togglePlay);
  fft = new p5.FFT();
  sound.amp(1);
  colorMode(HSB, 255)
}
var t = 0
var hue = 0
function draw(){
  if (t == 0){
    background(0,0,0);
  }
  if (sound.isPlaying()){
    let speclen = 256
    let spectrum = fft.analyze(speclen);
    for (let i = 0; i< speclen; i++){
      let x = map(i, 0, speclen, 0, width);
      let h = (-height + map(spectrum[i], 0, 255, height, 0)*1.5)*1.5;
      hue = map(h, height, 0, 255, -100)
      console.log(JSON.stringify(hue))
      stroke(-hue, 180, 0)
      noStroke();
      fill(-hue, 80, 180)
      rect(x + 5*noise(t), height, width / speclen + 5*noise(t), h*1.5)
      rect(width - x - 1 + 5*noise(t) , height, width / speclen + 5*noise(t), h*1.5)
      rect(x + 5*noise(t), 0, width / speclen + 5*noise(t), -h*1.5)
      rect(width - x - 1 + 5*noise(t) , 0, width / speclen + 5*noise(t), -h*1.5)
    }
    let wavlen=256
    let waveform = fft.waveform(wavlen);
    noFill();
    translate(width/2, height/2)
    rotate(PI * t)
    // beginShape();
    // stroke(20);
    // strokeWeight(13)
    // for (let k = 0; k < 4; k++){
    //   for (let i = 0; i < wavlen -1; i++){
    //     let x = map(i, 0, wavlen, 0, width);
    //     let thet = map(i, 0, wavlen, 0, HALF_PI)+ HALF_PI * k
    //     let y = map( waveform[i], -1, 1, 0, height/2);
    //     let pol = polar2cartesian(y*sin(t), thet)
    //     vertex(pol[0], pol[1]);
    //   }
    // }
    // endShape(CLOSE);
    beginShape();
    stroke(400*t % 255, 100, 180);
    strokeWeight(8)
    for (let k = 0; k < 4; k++){
      for (let i = 0; i < wavlen -1; i++){
        let x = map(i, 0, wavlen, 0, width);
        let thet = map(i, 0, wavlen, 0, HALF_PI)+ HALF_PI * k
        let y = map( waveform[i], -1, 1, 0, height/2);
        let pol = polar2cartesian(y*sin(t), thet)
        vertex(pol[0], pol[1]);
      }
    }
    endShape(CLOSE);
    rotate(-PI * t)
  }
  t += 0.01
  strokeWeight(1)
}

function togglePlay() {
  if (sound.isPlaying()) {
    sound.pause();
  } else {
    sound.loop();
  }
}
